import Vue from 'vue';
import App from './App.vue';
import router from './router';
import store from './store';
import './registerServiceWorker';

import './assets/main.scss';
import 'bootstrap';

import Navbar from './components/Navbar.vue';
import Footer from './components/Footer.vue';


import * as Sentry from '@sentry/browser';
import * as Integrations from '@sentry/integrations';

Vue.config.productionTip = false;

router.beforeEach((to, from, next) => {

  // This goes through the matched routes from last to first, finding the closest route with a title.
  // eg. if we have /some/deep/nested/route and /some, /deep, and /nested have titles, nested's will be chosen.
  const nearestWithTitle = to.matched.slice().reverse().find((r) => r.meta && r.meta.title);

  // If a route with a title was found, set the document (page) title to that value.
  if (nearestWithTitle) {
    document.title = 'Quote | ' + nearestWithTitle.meta.title;
  } else {
    document.title = 'Quote';
  }

  next();
});

new Vue({
  router,
  store,
  render: (h) => h(App),
}).$mount('#app');

Sentry.init({
  dsn: 'https://70c1f4f87cbc43c4b564d11e8ae7499b@sentry.io/1496519',
  integrations: [new Integrations.Vue({Vue, attachProps: true})],
});
