// tslint:disable:no-console

import Vue from 'vue';
import Vuex from 'vuex';

// @ts-ignore Ignore Vetur (https://vuejs.github.io/vetur/) error:
//
//   "Module '@/data/quotes.json' was resolved to '.../src/data/quotes.json',
//   but '--resolveJsonModule' is not used. Vetur(7042)".
//
// In order to use plain .json files in typescript whe need to add a .d.ts file
// to /src with:
//
//   declare module '*.json' {
//     const value: { [key: string]: any }
//     export default value
//   }
import quotes from '@/data/quotes.json';

Vue.use(Vuex);

export default new Vuex.Store({
  state: {
    quote: quotes[0],
  },
  mutations: {
    randomQuote(state) {
      state.quote = quotes[Math.floor(Math.random() * quotes.length)];
      console.log('"' + state.quote.text + '" - ' + state.quote.author + ' [' + state.quote.tags + ']');
    },
  },
  actions: {
    start(context) {
      setInterval(() => { context.commit('randomQuote'); }, 15 * 1000);
    },
  },
});
